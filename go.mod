module rev-shell-go

go 1.13

require (
	github.com/capnspacehook/taskmaster v0.0.0-20190802050140-eebf732b5748
	github.com/inancgumus/screen v0.0.0-20190314163918-06e984b86ed3
	github.com/rickb777/date v1.12.3
	github.com/robfig/cron/v3 v3.0.0 // indirect
	golang.org/x/crypto v0.0.0-20190907121410-71b5226ff739 // indirect
	golang.org/x/sys v0.0.0-20190412213103-97732733099d
)
