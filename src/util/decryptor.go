package util

import (
	"io"
	"time"
)

type Decryptor struct {
	data        []byte
	readIndex   int64
	sameCounter int64
	prevVal     int
}

func (dn *Decryptor) Read(p []byte) (n int, e error) {
	if dn.readIndex >= int64(len(p)) {
		return
	}

	data := dn.data[dn.readIndex:]

	//fmt.Println(dn.sameCounter)

	if len(data) > 0 {
		data = Xor(data, KEY)
		// data = XorRepeatingKey(data, KEY)
	}

	n = copy(p, data)

	if n == dn.prevVal {
		dn.sameCounter++
	} else {
		//fmt.Println(dn.sameCounter)
		dn.sameCounter = 0
	}

	if dn.sameCounter >= STOP {
		return 0, io.EOF
	}
	dn.prevVal = n

	//n = len(dn.data[dn.readIndex:])

	//fmt.Println(dn.zeroCounter)

	dn.readIndex += int64(n)
	time.Sleep(10 * time.Millisecond)
	return
}

func (dn *Decryptor) Write(p []byte) (n int, e error) {
	dn.sameCounter = 0
	dn.data = append(dn.data, p...)
	time.Sleep(10 * time.Millisecond)
	return len(p), nil

}

func NewDecryptor() *Decryptor {
	//key := fmt.Sprintf("%32v", KEY)
	//c, _ := aes.NewCipher([]byte(key))
	//gcm, _ := cipher.NewGCM(c)
	return &Decryptor{
		make([]byte, 0),
		0,
		0,
		0,
	}
}
