package util

import (
	"crypto/cipher"
	"time"
)

type Encryptor struct {
	data      []byte
	gcm       cipher.AEAD
	readIndex int64
}

func (en *Encryptor) Read(p []byte) (n int, e error) {
	time.Sleep(10 * time.Millisecond)
	if en.readIndex >= int64(len(p)) {
		return
	}
	//var data []byte = make([]byte, 0)
	//nonce := make([]byte, en.gcm.NonceSize())
	//if _, err := io.ReadFull(rand.Reader, nonce); err == nil && len(en.data[en.readIndex:]) > 0{
	//	data = en.gcm.Seal(nil, nonce, en.data[en.readIndex:], nil)
	//	fmt.Printf("%x\n", data)
	//	fmt.Printf("%x\n", nonce)
	//}
	data := en.data[en.readIndex:]
	//if len(data) > 0 {
	data = Xor(data, KEY)
	// data = XorRepeatingKey(data, KEY)
	//}
	n = copy(p, data)

	//n = len(en.data[en.readIndex:])
	en.readIndex += int64(n)
	return
}

func (en *Encryptor) Write(p []byte) (n int, e error) {
	en.data = append(en.data, p...)
	time.Sleep(10 * time.Millisecond)
	return len(p), nil

}

func NewEncryptor() *Encryptor {
	//key := fmt.Sprintf("%32v", KEY)
	//c, _ := aes.NewCipher([]byte(key))
	//gcm, _ := cipher.NewGCM(c)
	//key := KEY
	return &Encryptor{
		make([]byte, 0),
		nil,
		0,
	}
}
