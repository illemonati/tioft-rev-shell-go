package util

type DecryptorNoTimeout struct {
	data        []byte
	readIndex   int64
	sameCounter int64
	prevVal     int
}

func (dn *DecryptorNoTimeout) Read(p []byte) (n int, e error) {
	if dn.readIndex >= int64(len(p)) {
		return
	}

	data := dn.data[dn.readIndex:]

	if len(data) > 0 {
		data = Xor(data, KEY)
		// data = XorRepeatingKey(data, KEY)
	}

	n = copy(p, data)

	dn.readIndex += int64(n)
	return
}

func (dn *DecryptorNoTimeout) Write(p []byte) (n int, e error) {
	dn.sameCounter = 0
	dn.data = append(dn.data, p...)
	return len(p), nil
}

func NewDecryptorNoTimeout() *DecryptorNoTimeout {
	//key := fmt.Sprintf("%32v", KEY)
	//c, _ := aes.NewCipher([]byte(key))
	//gcm, _ := cipher.NewGCM(c)
	return &DecryptorNoTimeout{
		make([]byte, 0),
		0,
		0,
		0,
	}
}
