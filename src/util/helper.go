package util

func Xor(data []byte, key string) []byte {
	if len(data) > 0 {
		data2 := data
		for _, c := range key {
			cb := byte(c)
			data2 = XorSingle(data2, cb)
		}
		data = data2
	}
	return data
}

func XorRepeatingKey(data []byte, key string) []byte {
	keyIndex := 0
	if len(data) > 0 {
		var data2 []byte
		for b := range data {
			cb := byte(key[keyIndex])
			b2 := byte(byte(b) ^ cb)
			data2 = append(data2, b2)
			if keyIndex < len(key)-1 {
				keyIndex++
			} else {
				keyIndex = 0
			}
		}

		data = data2
	}
	return data
}

func XorSingle(data []byte, c byte) []byte {
	if len(data) > 0 {
		data2 := make([]byte, 0)
		for _, b := range data {
			data2 = append(data2, byte(b^c))
		}
		data = data2
	}
	return data
}
